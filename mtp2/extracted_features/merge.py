import numpy as np

log_feat = np.genfromtxt("logfb.csv",delimiter=",")
lpcc_feat = np.genfromtxt("lpcc_coeff.csv",delimiter=",")
plp_feat = np.genfromtxt("plp_coeff.csv",delimiter = ",")
rasta_feat = np.genfromtxt("rasta_coeff.csv",delimiter = ",")
rc_feat = np.genfromtxt("rc_coeff.csv",delimiter = ",")
ssc_feat = np.genfromtxt("ssc.csv",delimiter = ",")
st_feat = np.genfromtxt("st_features.csv",delimiter = ",")

x=list()
for i in range(1402):
	y=np.concatenate((st_feat[i],log_feat[i],lpcc_feat[i],plp_feat[i],rasta_feat[i],rc_feat[i],ssc_feat[i]),axis=1)
	x.append(y.tolist())
x=np.array(x)
np.savetxt("test_merged.csv",x,delimiter=",")