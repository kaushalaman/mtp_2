# Author: Aman Kaushal
# Research on emotion detection from audio implementation

import os, glob, ntpath, shutil
import scipy.io.wavfile as wavfile
import csv
from collections import OrderedDict
import itertools as IT

def convertDirMP3ToWav(dirName, Fs, nC):
	'''
	This function converts the MP3 files stored in a folder to WAV. If required, the output names of the WAV files are based on MP3 tags, otherwise the same names are used.
	ARGUMENTS:
	 - dirName:		the path of the folder where the MP3s are stored
	 - Fs:			the sampling rate of the generated WAV files
	 - nC:			the number of channesl of the generated WAV files
	'''

	types = (dirName+os.sep+'*.mp3',) # the tuple of file types
	filesToProcess = []

	for files in types:
		print files
		filesToProcess.extend(glob.glob(files))	
	for f in filesToProcess:
		wavFileName = f.replace(".mp3",".wav")		
		command = "avconv -i \"" + f + "\" -ar " +str(Fs) + " -ac " + str(nC) + " \"" + wavFileName + "\"";
		print command
		os.system(command.decode('unicode_escape').encode('ascii','ignore').replace("\0",""))

def convertFsDirWavToWav(dirName, Fs, nC):
	'''
	This function converts the WAV files stored in a folder to WAV using a different sampling freq and number of channels.
	ARGUMENTS:
	 - dirName:		the path of the folder where the WAVs are stored
	 - Fs:			the sampling rate of the generated WAV files
	 - nC:			the number of channesl of the generated WAV files
	'''

	types = (dirName+os.sep+'*.wav',) # the tuple of file types
	filesToProcess = []

	for files in types:
		filesToProcess.extend(glob.glob(files))		

	newDir = dirName + os.sep + "Fs" + str(Fs) + "_" + "NC"+str(nC)
	if os.path.exists(newDir) and newDir!=".":
		shutil.rmtree(newDir)	
	os.makedirs(newDir)	

	for f in filesToProcess:	
		_, wavFileName = ntpath.split(f)	
		command = "avconv -i \"" + f + "\" -ar " +str(Fs) + " -ac " + str(nC) + " \"" + newDir + os.sep + wavFileName + "\"";
		print command
		os.system(command)

def readAudioFile(path):
	'''
	This function returns a numpy array that stores the audio samples of a specified WAV of AIFF file
	'''
	extension = os.path.splitext(path)[1]

	try:
		if extension.lower() == '.wav':
			[Fs, x] = wavfile.read(path)
		elif extension.lower() == '.aif' or extension.lower() == '.aiff':
			s = aifc.open(path, 'r')
			nframes = s.getnframes()
			strsig = s.readframes(nframes)
			x = numpy.fromstring(strsig, numpy.short).byteswap()
			Fs = s.getframerate()
		else:
			print "Error in readAudioFile(): Unknown file type!"
			return (-1,-1)
	except IOError:	
		print "Error: file not found or other I/O error."
		return (-1,-1)
	return (Fs, x)

def stereo2mono(x):
	'''
	This function converts the input signal (stored in a numpy array) to MONO (if it is STEREO)
	'''
	if x.ndim==1:
		return x
	else:
		if x.ndim==2:
			return ( (x[:,1] / 2) + (x[:,0] / 2) )
		else:
			return -1

def mergecsvfiles(mypath):
	f = []
	for (dirpath, dirnames, filenames) in os.walk(mypath):
		f.extend(filenames)
		break
	print f, len(f)
	fout=open("out.csv","a")
	i=0
	# first file:
	for line in open(f[i]):
		fout.write(line)
	# now the rest:    
	for num in range(1,9):
		print str(f[num]) + " file opening"
    	fi = open(f[num])
    	fi.next() # skip the header
    	for line in fi:
        	fout.write(line)
    	fi.close() # not really needed
	fout.close()

def insert_id_column(mypath):
	filenames = []
	for (dirpath, dirnames, f) in os.walk(mypath):
		filenames.extend(f)
		break
	print filenames, len(filenames)
	for i in range(9):
		with open(filenames[i], 'rb') as inputs, open("new_"+filenames[i], 'wb') as output:
			reader = csv.reader(inputs, delimiter = ',')
			writer = csv.writer(output, delimiter = ',')

			all = []
			row = next(reader)
			row.insert(0, 'ID')
			all.append(row)
			for k, row in enumerate(reader):
				all.append([str(k+1)] + row)
			writer.writerows(all)

def merge_csv_attr_key(mypath):
	filenames = []
	for (dirpath, dirnames, f) in os.walk(mypath):
		filenames.extend(f)
		break
	print filenames, len(filenames)
	data = OrderedDict()
	fieldnames = []
	os.chdir(mypath)
	for filename in filenames:
		with open(str(filename), "rb") as fp: # python 2
			reader = csv.DictReader(fp)
			fieldnames.extend(reader.fieldnames)
			for row in reader:
				data.setdefault(row["id"], {}).update(row)

	fieldnames = list(OrderedDict.fromkeys(fieldnames))
	with open("merged.csv", "wb") as fp:
		writer = csv.writer(fp)
		writer.writerow(fieldnames)
		for row in data.itervalues():
			writer.writerow([row.get(field, '') for field in fieldnames])

def merge_csv_attr(mypath):
	filenames = []
	for (dirpath, dirnames, f) in os.walk(mypath):
		filenames.extend(f)
		break
	print filenames, len(filenames)
	handles = [open(filename, 'rb') for filename in filenames]    
	readers = [csv.reader(f, delimiter=',') for f in handles]

	with  open('combined.csv', 'wb') as h:
	    writer = csv.writer(h, delimiter=',', lineterminator='\n', )
	    for rows in IT.izip_longest(*readers, fillvalue=['']*2):
	        combined_row = []
	        for row in rows:
	            row = row[:2] # select the columns you want
	            if len(row) == 2:
	                combined_row.extend(row)
	            else:
	                combined.extend(['']*2)
	        writer.writerow(combined_row)

	for f in handles:
	    f.close()
