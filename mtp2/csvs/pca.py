import numpy as np
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
d= np.genfromtxt('/home/ayush/Desktop/mtp_2/mtp2/csvs/58dataset.csv',delimiter=",")
data = d[:,0:58]
print data, len(data[0])
target = d[:,58]
print target, len(target)
X_std = StandardScaler().fit_transform(data)
print len(X_std),len(X_std[0])
print X_std
pca = PCA(copy=True, n_components=58, whiten=False)
X=pca.fit_transform(X_std)
print len(X),len(X[0])

#PCA(copy=True, n_components=68, whiten=False)
np.savetxt("2_pca_dataset.csv",X,delimiter=",")
print(pca.explained_variance_ratio_)
print len(pca.explained_variance_ratio_)
