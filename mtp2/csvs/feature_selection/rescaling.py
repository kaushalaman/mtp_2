import numpy as np
from sklearn import preprocessing

d= np.genfromtxt('/home/ayush/Desktop/mtp_2/mtp2/csvs/69dataset.csv',delimiter=",")
data = d[:,0:68]
target = d[:,68]

normalized_data = preprocessing.normalize(data)

print normalized_data
print len(normalized_data)
print len(normalized_data[0])

np.savetxt("normalized_68dataset.csv",normalized_data , delimiter=",")

standardized_X = preprocessing.scale(data)

print standardized_X
print len(standardized_X)
print len(standardized_X[0])

np.savetxt("standardize_68dataset.csv",standardized_X , delimiter=",")
