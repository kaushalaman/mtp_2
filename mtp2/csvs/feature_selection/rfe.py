import numpy as np
from sklearn.feature_selection import RFE
from sklearn.linear_model import LogisticRegression

d= np.genfromtxt('/home/ayush/Desktop/mtp_2/mtp2/csvs/69dataset.csv',delimiter=",")
data = d[:,0:68]
target = d[:,68]

model = LogisticRegression()
rfe = RFE(model)
rfe = rfe.fit(data,target)
print(len(rfe.support_))

print(rfe.support_)

print(rfe.ranking_)

"""
		Result
RFE Ranking
[25  1  1  4  1  1 32  3  1  1  1  1  1  1  1  1  1  1  1  1  1 14 16  9 11
 27 34 21 30  6  5 20 28 31 26  1  1 33 29  1 22 12  1  1  1  1  2  1  1  1
  1  1  1  1  1 15 13 10  7 19 24 17 18  1  1  8 23 35]

RFE Support
[False  True  True False  True  True False False  True  True  True  True
  True  True  True  True  True  True  True  True  True False False False
 False False False False False False False False False False False  True
  True False False  True False False  True  True  True  True False  True
  True  True  True  True  True  True  True False False False False False
 False False False  True  True False False False]

"""
