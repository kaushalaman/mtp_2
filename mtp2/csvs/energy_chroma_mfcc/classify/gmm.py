print(__doc__)

# Author: Ron Weiss <ronweiss@gmail.com>, Gael Varoquaux
# License: BSD 3 clause

# $Id$

import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np

from sklearn import datasets
from sklearn.cross_validation import StratifiedKFold
from sklearn.externals.six.moves import xrange
from sklearn.mixture import GMM


d= np.genfromtxt('/home/ayush/Desktop/mtp_2/mtp2/csvs/energy_chroma_mfcc/58dataset.csv',delimiter=",")
data = d[:,0:58]
target = d[:,58]

# Break up the dataset into non-overlapping training (75%) and testing
# (25%) sets.
skf = StratifiedKFold(target, n_folds=4)

# Only take the first fold.
train_index, test_index = next(iter(skf))

X_train = data[train_index]
y_train = target[train_index]
X_test = data[test_index]
y_test = target[test_index]


n_classes = len(np.unique(y_train))

# Try GMMs using different types of covariances.
classifiers = dict((covar_type, GMM(n_components=n_classes,
                    covariance_type=covar_type, init_params='wc', n_iter=20))
                   for covar_type in ['spherical', 'diag', 'tied', 'full'])

n_classifiers = len(classifiers)


for index, (name, classifier) in enumerate(classifiers.items()):
    # Since we have class labels for the training data, we can
    # initialize the GMM parameters in a supervised manner.
    classifier.means_ = np.array([X_train[y_train == i].mean(axis=0)
                                  for i in xrange(n_classes)])

    # Train the other parameters using the EM algorithm.
    classifier.fit(X_train)    
    y_train_pred = classifier.predict(X_train)
    train_accuracy = np.mean(y_train_pred.ravel() == y_train.ravel()) * 100
    print "Training Accuracy  %.2f with classifier %s"%(train_accuracy,name)
   

    y_test_pred = classifier.predict(X_test)
    test_accuracy = np.mean(y_test_pred.ravel() == y_test.ravel()) * 100
    print "Testing Accuracy  %.2f with classifier %s"%(test_accuracy,name)
    
"""
pca

Training Accuracy  36.73 with classifier spherical
Testing Accuracy  32.35 with classifier spherical
Training Accuracy  44.39 with classifier diag
Testing Accuracy  22.06 with classifier diag
Training Accuracy  70.41 with classifier full
Testing Accuracy  27.94 with classifier full
Training Accuracy  58.16 with classifier tied
Testing Accuracy  30.88 with classifier tied


"""
