from sklearn import metrics
from sklearn.naive_bayes import GaussianNB

import numpy as np
from sklearn.metrics import accuracy_score
d= np.genfromtxt('/home/ayush/Desktop/mtp_2/mtp2/csvs/69dataset.csv',delimiter=",")
data = d[:,0:68]
target = d[:,68]

from sklearn.naive_bayes import MultinomialNB
model = MultinomialNB()
model.fit(data, target)

print model
# make predictions
expected = target
predicted = model.predict(data)
# summarize the fit of the model
print expected, predicted
# summarize the fit of the model
print(metrics.classification_report(expected, predicted))
print(metrics.confusion_matrix(expected, predicted))
print accuracy_score(expected, predicted)*100
print accuracy_score(expected, predicted, normalize=False)
