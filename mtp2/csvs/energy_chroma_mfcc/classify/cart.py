import numpy as np
from sklearn.metrics import accuracy_score
from sklearn import metrics

d= np.genfromtxt('/home/ayush/Desktop/mtp_2/mtp2/csvs/2_pca_dataset.csv',delimiter=",")
data = d[:,0:58]
target = d[:,58]

from sklearn.tree import DecisionTreeClassifier
model = DecisionTreeClassifier()
model.fit(data, target)
print(model)

# make predictions
expected = target
predicted = model.predict(data)
# summarize the fit of the model
print expected, predicted
# summarize the fit of the model
print(metrics.classification_report(expected, predicted))
print(metrics.confusion_matrix(expected, predicted))
print accuracy_score(expected, predicted)*100
print accuracy_score(expected, predicted, normalize=False)


"""
     1.00      1.00      1.00        81
        1.0       1.00      1.00      1.00        21
        2.0       1.00      1.00      1.00        84
        3.0       1.00      1.00      1.00        78

avg / total       1.00      1.00      1.00       264

[[81  0  0  0]
 [ 0 21  0  0]
 [ 0  0 84  0]
 [ 0  0  0 78]]
100.0
264
"""
