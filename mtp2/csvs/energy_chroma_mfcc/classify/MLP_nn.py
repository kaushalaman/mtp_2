import numpy as np
from sklearn.metrics import accuracy_score
from sklearn import metrics
from sknn.mlp import Classifier, Layer

from sklearn.pipeline import Pipeline
from sklearn.preprocessing import MinMaxScaler

d= np.genfromtxt('/home/ayush/Desktop/mtp_2/mtp2/csvs/energy_chroma_mfcc/58dataset.csv',delimiter=",")
data = d[:,0:58]
target = d[:,58]


pipeline = Pipeline([
        ('min/max scaler', MinMaxScaler(feature_range=(0.0, 1.0))),
        ('neural network', Classifier(layers=[Layer("Softmax")], n_iter=25))])
pipeline.fit(data, target)
#from sklearn.neural_network import MLPClassifier
#model = MLPClassifier(algorithm='l-bfgs', alpha=1e-5, hidden_layer_sizes=(5, 2), random_state=1)

#model.fit(data, target)

print(pipeline)

# make predictions
expected = target
predicted = pipeline.predict(data)
# summarize the fit of the model
print expected, predicted
# summarize the fit of the model
print(metrics.classification_report(expected, predicted))
print(metrics.confusion_matrix(expected, predicted))
print accuracy_score(expected, predicted)*100
print accuracy_score(expected, predicted, normalize=False) 

"""
58dataset
60.9848484848
=========
pca
precision    recall  f1-score   support

        0.0       0.76      0.54      0.63        81
        1.0       0.00      0.00      0.00        21
        2.0       0.43      0.90      0.58        84
        3.0       0.73      0.28      0.41        78

avg / total       0.59      0.54      0.50       264

[[44  0 34  3]
 [ 1  0 18  2]
 [ 5  0 76  3]
 [ 8  0 48 22]]
53.7878787879
142
=======================================

precision    recall  f1-score   support

        0.0       0.56      0.59      0.58        81
        1.0       0.00      0.00      0.00        21
        2.0       0.46      0.71      0.56        84
        3.0       0.51      0.31      0.38        78

avg / total       0.47      0.50      0.47       264

[[48  1 21 11]
 [ 4  0 14  3]
 [15  0 60  9]
 [18  0 36 24]]
50.0
132

"""
