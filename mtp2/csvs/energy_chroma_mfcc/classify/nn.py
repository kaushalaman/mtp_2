from sknn.mlp import Classifier, Layer
import numpy as np
d= np.genfromtxt('/home/ayush/Desktop/mtp_2/mtp2/csvs/energy_chroma_mfcc/58dataset.csv',delimiter=",")

data = d[:,0:58]
target = d[:,58]

test_data = d[0:213,0:58]
test_target = d[0:213,58]
nn = Classifier(
    layers=[
        Layer("Rectifier", units=100),
        Layer("Softmax")],
    learning_rate=0.02,
    n_iter=10)


nn.fit(data, target)

#y_valid = nn.predict(X_valid)

score = nn.score(data, target)
#test_score = nn.score(test_data, test_target)

print("Accuracy: %f" % (score*100.0))
#print("Accuracy: %f" % (test_score*100.0))

"""
pca
Accuracy: 97.727273
===============
norm,58
Accuracy: 31.818182
==============
standard
Accuracy: 86.363636


"""
