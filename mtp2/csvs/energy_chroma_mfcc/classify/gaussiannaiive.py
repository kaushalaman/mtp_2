from sklearn import metrics
from sklearn.naive_bayes import GaussianNB

import numpy as np
from sklearn.metrics import accuracy_score
d= np.genfromtxt('/home/ayush/Desktop/mtp_2/mtp2/csvs/energy_chroma_mfcc/58dataset.csv',delimiter=",")
data = d[:,0:58]
target = d[:,58]

from sklearn.naive_bayes import GaussianNB

model = GaussianNB()
model.fit(data, target)

print model
# make predictions
expected = target
predicted = model.predict(data)
# summarize the fit of the model
print expected, predicted
# summarize the fit of the model
print(metrics.classification_report(expected, predicted))
print(metrics.confusion_matrix(expected, predicted))
print accuracy_score(expected, predicted)*100
print accuracy_score(expected, predicted, normalize=False)

"""
pca
precision    recall  f1-score   support

        0.0       0.75      0.79      0.77        81
        1.0       0.79      0.71      0.75        21
        2.0       0.74      0.70      0.72        84
        3.0       0.65      0.67      0.66        78

avg / total       0.72      0.72      0.72       264

[[64  0  7 10]
 [ 1 15  1  4]
 [ 8  3 59 14]
 [12  1 13 52]]
71.9696969697
190

=============================================
norm
precision    recall  f1-score   support

        0.0       0.60      0.59      0.60        81
        1.0       0.34      0.71      0.46        21
        2.0       0.49      0.56      0.53        84
        3.0       0.62      0.36      0.46        78

avg / total       0.55      0.52      0.52       264

[[48  8 16  9]
 [ 2 15  2  2]
 [18 13 47  6]
 [12  8 30 28]]
52.2727272727
138

========================
standard
31.8181818182
==============
58dataset
52.6515151515
"""
