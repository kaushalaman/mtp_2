from sklearn import metrics
from sklearn.linear_model import LogisticRegression
import numpy as np
from sklearn.metrics import accuracy_score
d= np.genfromtxt('/home/ayush/Desktop/mtp_2/mtp2/csvs/energy_chroma_mfcc/58dataset.csv',delimiter=",")
data = d[:,0:58]
target = d[:,58]

model = LogisticRegression()
model.fit(data, target)

print(model)

# make predictions
expected = target
predicted = model.predict(data)
print expected, predicted
# summarize the fit of the model
print(metrics.classification_report(expected, predicted))
print(metrics.confusion_matrix(expected, predicted))
print accuracy_score(expected, predicted)*100
print accuracy_score(expected, predicted, normalize=False)

"""
pca
precision    recall  f1-score   support

        0.0       0.71      0.78      0.74        81
        1.0       0.77      0.48      0.59        21
        2.0       0.65      0.71      0.68        84
        3.0       0.72      0.64      0.68        78

avg / total       0.70      0.69      0.69       264

[[63  0 11  7]
 [ 3 10  6  2]
 [12  2 60 10]
 [11  1 16 50]]
69.3181818182
183
===================
norm
precision    recall  f1-score   support

        0.0       0.00      0.00      0.00        81
        1.0       0.00      0.00      0.00        21
        2.0       0.32      1.00      0.48        84
        3.0       0.00      0.00      0.00        78

avg / total       0.10      0.32      0.15       264

[[ 0  0 81  0]
 [ 0  0 21  0]
 [ 0  0 84  0]
 [ 0  0 78  0]]
31.8181818182
84
======
stand
31
===============
58dataset
44

"""
