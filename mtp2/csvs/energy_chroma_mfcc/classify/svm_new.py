import numpy as np
from sklearn.metrics import accuracy_score
from sklearn import metrics

d= np.genfromtxt('/home/ayush/Desktop/mtp_2/mtp2/csvs/energy_chroma_mfcc/58dataset.csv',delimiter=",")
data = d[:,0:58]
target = d[:,58]
from sklearn.svm import SVC
model = SVC()
model.fit(data, target)
print(model)

# make predictions
expected = target
predicted = model.predict(data)
# summarize the fit of the model
print expected, predicted
# summarize the fit of the model
print(metrics.classification_report(expected, predicted))
print(metrics.confusion_matrix(expected, predicted))
print accuracy_score(expected, predicted)*100
print accuracy_score(expected, predicted, normalize=False)

"""
58data
37
=====================
pca
precision    recall  f1-score   support

        0.0       0.78      0.89      0.83        81
        1.0       1.00      0.29      0.44        21
        2.0       0.76      0.88      0.82        84
        3.0       0.88      0.78      0.83        78

avg / total       0.82      0.81      0.80       264

[[72  0  6  3]
 [ 5  6  7  3]
 [ 8  0 74  2]
 [ 7  0 10 61]]
80.6818181818
213
=========================================
standard
78.7878787879
==============
norm
31


"""
