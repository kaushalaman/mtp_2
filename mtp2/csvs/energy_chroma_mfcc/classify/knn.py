from sklearn import metrics
import numpy as np
from sklearn.metrics import accuracy_score
from sklearn import metrics

d= np.genfromtxt('/home/ayush/Desktop/mtp_2/mtp2/csvs/energy_chroma_mfcc/58dataset.csv',delimiter=",")
data = d[:,0:58]
target = d[:,58]

from sklearn.neighbors import KNeighborsClassifier
model = KNeighborsClassifier()
model.fit(data, target)
print(model)


# make predictions
expected = target
predicted = model.predict(data)
# summarize the fit of the model
print expected, predicted
# summarize the fit of the model
print(metrics.classification_report(expected, predicted))
print(metrics.confusion_matrix(expected, predicted))
print accuracy_score(expected, predicted)*100
print accuracy_score(expected, predicted, normalize=False)
"""
58dataset
60.9848484848
=====================
pca
precision    recall  f1-score   support

        0.0       0.62      0.85      0.72        81
        1.0       0.67      0.57      0.62        21
        2.0       0.70      0.65      0.67        84
        3.0       0.73      0.53      0.61        78

avg / total       0.68      0.67      0.67       264

[[69  2  3  7]
 [ 4 12  3  2]
 [21  2 55  6]
 [17  2 18 41]]
67.0454545455
177
====================
stnadrd
61.7424242424

====================================
norm
precision    recall  f1-score   support

        0.0       0.54      0.77      0.63        81
        1.0       0.58      0.52      0.55        21
        2.0       0.65      0.54      0.59        84
        3.0       0.64      0.50      0.56        78

avg / total       0.61      0.59      0.59       264

[[62  4 10  5]
 [ 8 11  0  2]
 [22  2 45 15]
 [23  2 14 39]]
59.4696969697
157
"""

