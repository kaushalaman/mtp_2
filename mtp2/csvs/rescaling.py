import numpy as np
from sklearn import preprocessing

d= np.genfromtxt('/home/ayush/Desktop/mtp_2/mtp2/csvs/2_mean_cunhappy.csv',delimiter=",")
data = d[:,0:58]
target = d[:,58]

normalized_data = preprocessing.normalize(data)

print normalized_data
print len(normalized_data)
print len(normalized_data[0])

np.savetxt("2_normalized_unhappy.csv",normalized_data , delimiter=",")

standardized_X = preprocessing.scale(data)

print standardized_X
print len(standardized_X)
print len(standardized_X[0])

np.savetxt("2_standard_unhappy.csv",standardized_X , delimiter=",")
