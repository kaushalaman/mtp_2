import numpy as np
from sklearn.metrics import accuracy_score
from sklearn import metrics
from sknn.mlp import Classifier, Layer

from sklearn.pipeline import Pipeline
from sklearn.preprocessing import MinMaxScaler

d= np.genfromtxt('/home/ayush/Desktop/mtp_2/mtp2/csvs/pca_dataset.csv',delimiter=",")
data = d[:,0:68]
target = d[:,68]


pipeline = Pipeline([
        ('min/max scaler', MinMaxScaler(feature_range=(0.0, 1.0))),
        ('neural network', Classifier(layers=[Layer("Softmax")], n_iter=25))])
pipeline.fit(data, target)
#from sklearn.neural_network import MLPClassifier
#model = MLPClassifier(algorithm='l-bfgs', alpha=1e-5, hidden_layer_sizes=(5, 2), random_state=1)

#model.fit(data, target)

print(pipeline)

# make predictions
expected = target
predicted = pipeline.predict(data)
# summarize the fit of the model
print expected, predicted
# summarize the fit of the model
print(metrics.classification_report(expected, predicted))
print(metrics.confusion_matrix(expected, predicted))
print accuracy_score(expected, predicted)*100
print accuracy_score(expected, predicted, normalize=False) 

"""
69dataset.csv
precision    recall  f1-score   support

        0.0       0.58      0.52      0.55        81
        1.0       0.00      0.00      0.00        21
        2.0       0.48      0.71      0.57        84
        3.0       0.54      0.44      0.48        78

avg / total       0.49      0.52      0.49       264

[[42  2 23 14]
 [ 5  0 13  3]
 [12  0 60 12]
 [14  0 30 34]]
51.5151515152
136
==========================================================

pca
precision    recall  f1-score   support

        0.0       0.49      0.93      0.64        81
        1.0       0.00      0.00      0.00        21
        2.0       0.79      0.54      0.64        84
        3.0       0.70      0.49      0.58        78

avg / total       0.61      0.60      0.57       264

[[75  0  3  3]
 [13  0  3  5]
 [31  0 45  8]
 [34  0  6 38]]
59.8484848485
158

"""
