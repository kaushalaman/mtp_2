import numpy as np
from sklearn import cross_validation
from sklearn import svm

d= np.genfromtxt('/home/ayush/Desktop/mtp_2/mtp2/csvs/pca_dataset.csv',delimiter=",")
data = d[:,0:68]
target = d[:,68]

X_train, X_test, y_train, y_test = cross_validation.train_test_split(data, target, test_size=0.4, random_state=0)
clf = svm.SVC(kernel='linear', C=1).fit(X_train, y_train)
print clf.score(X_test, y_test)  

clf = svm.SVC(kernel='linear', C=1)
scores = cross_validation.cross_val_score(clf, data, target, cv=5)
print "cross validation scores", scores

print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))



print "After preprocessing "
from sklearn import metrics
from sklearn import preprocessing
X_train, X_test, y_train, y_test = cross_validation.train_test_split(data, target, test_size=0.4, random_state=0)
scaler = preprocessing.StandardScaler().fit(X_train)
X_train_transformed = scaler.transform(X_train)
clf = svm.SVC(C=1).fit(X_train_transformed, y_train)
X_test_transformed = scaler.transform(X_test)
print clf.score(X_test_transformed, y_test)


from sklearn.pipeline import make_pipeline
clf = make_pipeline(preprocessing.StandardScaler(), svm.SVC(C=1))
print cross_validation.cross_val_score(clf, data, target)

predicted = cross_validation.cross_val_predict(clf, data, target, cv=10)
print "accuracy",metrics.accuracy_score(target, predicted) 


