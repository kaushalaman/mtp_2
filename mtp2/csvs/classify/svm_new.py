import numpy as np
from sklearn.metrics import accuracy_score
from sklearn import metrics

d= np.genfromtxt('/home/ayush/Desktop/mtp_2/mtp2/csvs/pca_dataset.csv',delimiter=",")
data = d[:,0:68]
target = d[:,68]
from sklearn.svm import SVC
model = SVC()
model.fit(data, target)
print(model)

# make predictions
expected = target
predicted = model.predict(data)
# summarize the fit of the model
print expected, predicted
# summarize the fit of the model
print(metrics.classification_report(expected, predicted))
print(metrics.confusion_matrix(expected, predicted))
print accuracy_score(expected, predicted)*100
print accuracy_score(expected, predicted, normalize=False)

"""
69dataset
 precision    recall  f1-score   support

        0.0       0.37      0.58      0.45        81
        1.0       0.00      0.00      0.00        21
        2.0       0.35      0.57      0.43        84
        3.0       0.00      0.00      0.00        78

avg / total       0.23      0.36      0.28       264

[[47  0 34  0]
 [12  0  9  0]
 [36  0 48  0]
 [32  0 46  0]]
35.9848484848
95
=========================================

=========================================================
standard_datset
precision    recall  f1-score   support

        0.0       0.87      0.80      0.83        81
        1.0       0.00      0.00      0.00        21
        2.0       0.69      0.98      0.81        84
        3.0       0.89      0.79      0.84        78

avg / total       0.75      0.79      0.76       264

[[65  0 13  3]
 [ 4  0 14  3]
 [ 0  0 82  2]
 [ 6  0 10 62]]
79.1666666667
209
==============================
pca
precision    recall  f1-score   support

        0.0       0.79      0.90      0.84        81
        1.0       1.00      0.38      0.55        21
        2.0       0.78      0.87      0.82        84
        3.0       0.86      0.77      0.81        78

avg / total       0.82      0.81      0.80       264

[[73  0  5  3]
 [ 3  8  6  4]
 [ 8  0 73  3]
 [ 8  0 10 60]]
81.0606060606
214

"""
