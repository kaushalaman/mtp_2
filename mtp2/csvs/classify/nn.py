from sknn.mlp import Classifier, Layer
import numpy as np
d= np.genfromtxt('/home/ayush/Desktop/mtp_2/mtp2/csvs/pca_dataset.csv',delimiter=",")
data = d[:,0:68]
target = d[:,68]

test_data = d[0:213,0:68]
test_target = d[0:213,68]
nn = Classifier(
    layers=[
        Layer("Rectifier", units=100),
        Layer("Softmax")],
    learning_rate=0.02,
    n_iter=10)


nn.fit(data, target)

#y_valid = nn.predict(X_valid)

score = nn.score(data, target)
#test_score = nn.score(test_data, test_target)

print("Accuracy: %f" % (score*100.0))
#print("Accuracy: %f" % (test_score*100.0))

"""
standard
Accuracy: 88.636364
============================
pca

Accuracy: 93.939394
"""
