from sklearn import metrics
from sklearn.naive_bayes import GaussianNB

import numpy as np
from sklearn.metrics import accuracy_score
d= np.genfromtxt('/home/ayush/Desktop/mtp_2/mtp2/csvs/pca_dataset.csv',delimiter=",")
data = d[:,0:68]
target = d[:,68]

model = GaussianNB()
model.fit(data, target)
print(model)
# make predictions
expected = target
predicted = model.predict(data)
# summarize the fit of the model
print expected, predicted
# summarize the fit of the model
print(metrics.classification_report(expected, predicted))
print(metrics.confusion_matrix(expected, predicted))
print accuracy_score(expected, predicted)*100
print accuracy_score(expected, predicted, normalize=False)

"""
precision    recall  f1-score   support

        0.0       0.57      0.62      0.60        81
        1.0       0.30      0.67      0.41        21
        2.0       0.58      0.51      0.54        84
        3.0       0.55      0.40      0.46        78

avg / total       0.55      0.52      0.53       264

[[50 10 10 11]
 [ 2 14  2  3]
 [18 12 43 11]
 [17 11 19 31]]
52.2727272727
138

=============================================
pca
precision    recall  f1-score   support

        0.0       0.77      0.80      0.79        81
        1.0       0.84      0.76      0.80        21
        2.0       0.72      0.75      0.74        84
        3.0       0.76      0.72      0.74        78

avg / total       0.76      0.76      0.76       264

[[65  0  9  7]
 [ 1 16  1  3]
 [10  3 63  8]
 [ 8  0 14 56]]
75.7575757576
200
"""
