from sklearn import metrics
import numpy as np
from sklearn.metrics import accuracy_score
from sklearn import metrics

d= np.genfromtxt('/home/ayush/Desktop/mtp_2/mtp2/csvs/pca_dataset.csv',delimiter=",")
data = d[:,0:68]
target = d[:,68]

from sklearn.neighbors import KNeighborsClassifier
model = KNeighborsClassifier()
model.fit(data, target)
print(model)


# make predictions
expected = target
predicted = model.predict(data)
# summarize the fit of the model
print expected, predicted
# summarize the fit of the model
print(metrics.classification_report(expected, predicted))
print(metrics.confusion_matrix(expected, predicted))
print accuracy_score(expected, predicted)*100
print accuracy_score(expected, predicted, normalize=False)
"""
69dataset.csv
 precision    recall  f1-score   support

        0.0       0.56      0.70      0.62        81
        1.0       0.65      0.52      0.58        21
        2.0       0.59      0.56      0.57        84
        3.0       0.66      0.55      0.60        78

avg / total       0.61      0.60      0.60       264

[[57  1 15  8]
 [ 3 11  3  4]
 [24  3 47 10]
 [18  2 15 43]]
59.8484848485
158
========================================
norm_dataset

precision    recall  f1-score   support

        0.0       0.54      0.78      0.64        81
        1.0       0.61      0.52      0.56        21
        2.0       0.69      0.56      0.62        84
        3.0       0.62      0.49      0.55        78

avg / total       0.62      0.60      0.60       264

[[63  3  7  8]
 [ 8 11  0  2]
 [22  2 47 13]
 [24  2 14 38]]
60.2272727273
159
===========================================

standard_dataset
        0.0       0.60      0.74      0.66        81
        1.0       0.40      0.10      0.15        21
        2.0       0.57      0.70      0.63        84
        3.0       0.64      0.45      0.53        78

avg / total       0.58      0.59      0.57       264

[[60  0 13  8]
 [ 8  2  6  5]
 [15  3 59  7]
 [17  0 26 35]]
59.0909090909

===========================================
pca
 precision    recall  f1-score   support

        0.0       0.61      0.84      0.71        81
        1.0       0.64      0.43      0.51        21
        2.0       0.71      0.63      0.67        84
        3.0       0.70      0.58      0.63        78

avg / total       0.67      0.66      0.66       264

[[68  2  2  9]
 [ 4  9  4  4]
 [23  2 53  6]
 [16  1 16 45]]
66.2878787879
175
156

"""

